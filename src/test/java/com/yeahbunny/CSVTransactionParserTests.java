package com.yeahbunny;

import com.yeahbunny.exception.ParseException;
import com.yeahbunny.model.Transaction;
import com.yeahbunny.service.CSVTransactionParser;
import com.yeahbunny.service.TransactionParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class CSVTransactionParserTests {

    @Test
    public void successParse() {
        String[] args = {"1","trip","120","10","PLN","true"};

        Transaction transaction = new CSVTransactionParser().parse(args);

        assertEquals(Long.parseLong(args[0]), transaction.getId());
        assertEquals(args[1], transaction.getType());
        assertEquals(Integer.parseInt(args[2]), transaction.getPrice());
        assertEquals(Integer.parseInt(args[3]), transaction.getCommission());
        assertEquals(args[4], transaction.getCurrency());
        assertEquals(Boolean.parseBoolean(args[5]), transaction.isPaid());

    }

    @Test(expected = ParseException.class)
    public void failParseTooFewValuesCount() {
        String[] args = {"1","trip"};

        Transaction transaction = new CSVTransactionParser().parse(args);

        assertNull(transaction);
    }

    @Test(expected = ParseException.class)
    public void failParseTooManyValuesCount() {
        String[] args = {"1","trip","120","10","PLN","true", "asd"};

        Transaction transaction = new CSVTransactionParser().parse(args);

        assertNull(transaction);
    }

    @Test(expected = ParseException.class)
    public void failParseNumberFormat() {
        String[] args = {"1","trip","asd","asd","PLN","true"};

        Transaction transaction = new CSVTransactionParser().parse(args);

        assertNull(transaction);
    }
}
