package com.yeahbunny;

import com.yeahbunny.dao.TransactionsCSVDao;
import com.yeahbunny.exception.DatabaseFormatException;
import com.yeahbunny.exception.DatabaseNotFoundException;
import com.yeahbunny.model.Transaction;
import com.yeahbunny.service.CSVTransactionParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
public class TransactionsCSVDaoTests {

    @Spy
    private CSVTransactionParser csvTransactionParser = new CSVTransactionParser();

    @InjectMocks
    private TransactionsCSVDao dao;


    @Test(expected=DatabaseNotFoundException.class)
    public void databaseNotFound(){

        ReflectionTestUtils.setField(dao,"databaseFilePath","worng/path");

        dao.findAllByCurrencyAndType("PLN","trip");
    }

    @Test(expected = DatabaseFormatException.class)
    public void findAllByCurrencyAndTypeInvalidDBTest(){

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("csvdatabase_tests_invalid").getFile());
        ReflectionTestUtils.setField(dao,"databaseFilePath",file.getAbsolutePath());

        dao.findAllByCurrencyAndType("PLN","trip");

    }

    @Test()
    public void findAllByCurrencyAndTypeTest(){

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("csvdatabase_tests").getFile());
        ReflectionTestUtils.setField(dao,"databaseFilePath",file.getAbsolutePath());

        List<Transaction> expected = new ArrayList<>();
        Transaction transaction = new Transaction();
        transaction.setId(3);
        transaction.setType("trip");
        transaction.setPrice(80);
        transaction.setCommission(20);
        transaction.setCurrency("PLN");
        transaction.setPaid(false);
        expected.add(transaction);

        Transaction transaction2 = new Transaction();
        transaction2.setId(6);
        transaction2.setType("trip");
        transaction2.setPrice(120);
        transaction2.setCommission(5);
        transaction2.setCurrency("PLN");
        transaction2.setPaid(true);
        expected.add(transaction2);

        List<Transaction> result = dao.findAllByCurrencyAndType("PLN","trip");

        assertEquals(result.size(),expected.size());

        for( int i =0; i< expected.size(); i++){
            assertEquals(result.get(i).getId(), expected.get(i).getId());
            assertEquals(result.get(i).getType(), expected.get(i).getType());
            assertEquals(result.get(i).getPrice(), expected.get(i).getPrice());
            assertEquals(result.get(i).getCommission(), expected.get(i).getCommission());
            assertEquals(result.get(i).isPaid(), expected.get(i).isPaid());
        }
    }

    @Test()
    public void findAllByCurrencyAndTypeEmptyTest(){
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("csvdatabase_tests").getFile());
        ReflectionTestUtils.setField(dao,"databaseFilePath",file.getAbsolutePath());

        List<Transaction> result = dao.findAllByCurrencyAndType("USD","trip");

        assertTrue(result.isEmpty());
    }


}
