package com.yeahbunny;

import com.yeahbunny.model.Transaction;
import com.yeahbunny.model.TransactionsReport;
import com.yeahbunny.service.SimpleTransactionsReportGenerator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class SimpleTransactionsReportGeneratorTest {

    @Test
    public void generateTest(){
        List<Transaction> data = new ArrayList<>();
        Transaction transaction = new Transaction();
        transaction.setId(3);
        transaction.setType("trip");
        transaction.setPrice(80);
        transaction.setCommission(20);
        transaction.setCurrency("PLN");
        transaction.setPaid(false);
        data.add(transaction);

        Transaction transaction2 = new Transaction();
        transaction2.setId(6);
        transaction2.setType("trip");
        transaction2.setPrice(120);
        transaction2.setCommission(5);
        transaction2.setCurrency("PLN");
        transaction2.setPaid(false);
        data.add(transaction2);

        Transaction transaction3 = new Transaction();
        transaction3.setId(7);
        transaction3.setType("trip");
        transaction3.setPrice(10);
        transaction3.setCommission(5);
        transaction3.setCurrency("PLN");
        transaction3.setPaid(true);
        data.add(transaction3);

        SimpleTransactionsReportGenerator generator = new SimpleTransactionsReportGenerator();

        TransactionsReport report = generator.generate(data);

        assertEquals("trip",report.getType());
        assertEquals("PLN", report.getCurrency());
        assertEquals(210, report.getPrice());
        assertEquals(30, report.getCommission());
        assertEquals(200, report.getToCharge());
        assertEquals(-20,report.getSettlement());
    }

}
