package com.yeahbunny.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TransactionsReport {
    private String currency;
    private String type;
    private int price;
    private int commission;
    @JsonProperty(value = "to_charge_value")
    private int toCharge;
    @JsonProperty(value = "settlement_value")
    private int settlement;

    public TransactionsReport() {
    }

    public TransactionsReport(String currency, String type, int price, int commission, int toCharge, int settlement) {
        this.currency = currency;
        this.type = type;
        this.price = price;
        this.commission = commission;
        this.toCharge = toCharge;
        this.settlement = settlement;
    }

    public String getCurrency() {
        return currency;
    }

    public String getType() {
        return type;
    }

    public int getPrice() {
        return price;
    }

    public int getCommission() {
        return commission;
    }

    public int getToCharge() {
        return toCharge;
    }

    public int getSettlement() {
        return settlement;
    }
}
