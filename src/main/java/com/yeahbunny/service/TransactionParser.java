package com.yeahbunny.service;

import com.yeahbunny.model.Transaction;

public interface TransactionParser<IN> extends Parser<IN,Transaction>{
}
