package com.yeahbunny.service;

import com.yeahbunny.model.Transaction;
import com.yeahbunny.model.TransactionsReport;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class SimpleTransactionsReportGenerator implements TransactionsReportGenerator {

    @Override
    public TransactionsReport generate(List<Transaction> transactionList) {
        if(transactionList == null || transactionList.size() == 0){
            return new TransactionsReport();
        }

        int price = 0, commission = 0, toCharge = 0;
        for(Transaction item:transactionList){

            price +=item.getPrice();
            commission += item.getCommission();
            if(!item.isPaid()){
                toCharge += item.getPrice();
            }

        }
        int settlement = price - commission - toCharge;
        TransactionsReport result = new TransactionsReport(transactionList.get(0).getCurrency(),
                transactionList.get(0).getType(),
                price,
                commission,
                toCharge,
                settlement);
        return result;
    }
}
