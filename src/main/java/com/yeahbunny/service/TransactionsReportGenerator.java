package com.yeahbunny.service;

import com.yeahbunny.model.Transaction;
import com.yeahbunny.model.TransactionsReport;

import java.util.List;

public interface TransactionsReportGenerator {
    TransactionsReport generate(List<Transaction> transactionList);
}
