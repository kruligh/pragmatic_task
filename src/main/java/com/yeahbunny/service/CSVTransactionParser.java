package com.yeahbunny.service;

import com.yeahbunny.exception.ParseException;
import com.yeahbunny.model.Transaction;
import org.springframework.stereotype.Component;

@Component
public class CSVTransactionParser implements TransactionParser<String[]> {

    private static final int transactionPropertiesCount;

    static {
        transactionPropertiesCount = Transaction.class.getDeclaredFields().length;
    }

    @Override
    public Transaction parse(String[] values) throws ParseException {
        if(values.length == transactionPropertiesCount){
            try{
                Transaction result = new Transaction();
                result.setId(Long.parseLong(values[0]));
                result.setType(values[1]);
                result.setPrice(Integer.parseInt(values[2]));
                result.setCommission(Integer.parseInt(values[3]));
                result.setCurrency(values[4]);
                result.setPaid(Boolean.parseBoolean(values[5]));
                return result;
            }catch (NumberFormatException e){
                throw new ParseException();
            }
        }else{
            throw new ParseException();
        }
    }
}
