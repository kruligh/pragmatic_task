package com.yeahbunny.service;

import com.yeahbunny.exception.ParseException;

public interface Parser<IN, OUT> {
    OUT parse(IN values) throws ParseException;
}
