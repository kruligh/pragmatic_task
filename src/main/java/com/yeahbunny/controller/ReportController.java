package com.yeahbunny.controller;

import com.yeahbunny.dao.TransactionsDao;
import com.yeahbunny.model.Transaction;
import com.yeahbunny.model.TransactionsReport;
import com.yeahbunny.service.TransactionsReportGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class ReportController {

    @Qualifier("simpleTransactionsReportGenerator")
    @Autowired
    private TransactionsReportGenerator reportGenerator;

    @Qualifier("transactionsCSVDao")
    @Autowired
    private TransactionsDao transactionsDao;

    @RequestMapping(value = "/report", method = RequestMethod.GET)
    public TransactionsReport getInfo(@RequestParam String currency, @RequestParam String type){

        List<Transaction> transactionList = transactionsDao.findAllByCurrencyAndType(currency,type);

        TransactionsReport transactionsReport = reportGenerator.generate(transactionList);

        return transactionsReport;

    }
}
