package com.yeahbunny.dao;

import com.yeahbunny.model.Transaction;

import java.util.List;

public interface TransactionsDao {

    List<Transaction> findAllByCurrencyAndType(String currency, String type);
}
