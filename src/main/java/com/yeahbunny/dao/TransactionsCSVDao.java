package com.yeahbunny.dao;

import com.opencsv.CSVReader;
import com.yeahbunny.exception.DatabaseFormatException;
import com.yeahbunny.exception.DatabaseNotFoundException;
import com.yeahbunny.exception.ParseException;
import com.yeahbunny.model.Transaction;
import com.yeahbunny.service.TransactionParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class TransactionsCSVDao implements TransactionsDao{

    @Value("${app.databasePath}")
    private String databaseFilePath;

    @Qualifier("CSVTransactionParser")
    @Autowired
    private TransactionParser<String[]> parser;



    @Override
    public List<Transaction> findAllByCurrencyAndType(String currency, String type) {
        try (CSVReader reader = new CSVReader(new FileReader(databaseFilePath),',', '\'')) {
            List<Transaction> resultList = new ArrayList<>();

            reader.forEach( values->{
                if(values.length > 4 && values[1].equals(type) && values[4].equals(currency)){
                    try{
                        resultList.add(parser.parse(values));
                    }catch (ParseException e){
                        throw new DatabaseFormatException();
                    }
                }
            });
            return resultList;

        }catch (IOException e){
            e.printStackTrace();
            throw new DatabaseNotFoundException();
        }
    }
}
